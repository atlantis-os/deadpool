# Устанавливаем

```
zypper in mkdocs-doc
```
или
```
pip install --upgrade pip
pip install mkdocs
pip install mkdocs-windmill - компактная тема
```



# Копируем



# Запускаем

```
mkdocs serve --dev-addr 0.0.0.0:80 --theme windmill
```

###### Делаем службу

```
vim /etc/systemd/system/mkdocs.service 
```

```
[Unit]
Description=MKDocs
After=network.target

[Service]
Type=simple
User=root
Group=root
TimeoutStartSec=0
Restart=on-failure
RestartSec=10
RemainAfterExit=yes
WorkingDirectory=<путь к диретории deadpool>
ExecStart=/bin/mkdocs serve --dev-addr 0.0.0.0:80 --theme readthedocs

[Install]
WantedBy=multi-user.target
```

```
systemd enable mkdocs
systemd restart mkdocs
```

```
http://127.0.0.1:80
```

