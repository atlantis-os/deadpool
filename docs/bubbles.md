* Лопнутые пузыри

```
В твоём подвале одни лопнутые пузыри
Как и эти девочки с тобой на всех гран при...
Поцелуйчики и обнимашки из под сцены...
А эти за толпой без эмоциональные маникены, а может голубки...
Как и всторонке стоящие пальцем показывают злые языки...
- Никогда не выходи из темноты...
---
Не говори то чего не сделаешь, врубая в обратку...
Не поджигай фитиль, (держа в руках взрывчатку...) когда стоишь рядом со взрывчаткой...
---
Ты разволил мне волну, я тебя за это согну...
Один скидывает стрелки на жену,
Другого жена от драки уводит за ушко
- поставь этому заглушку...
А этому объясни что микрофон на сцене не макушка!
Как и рукопожатия со звездой ему не делает чести...
И такие пробросы на речах очень часто попадают в вести...
---
Все что вы можете, - задевать из подтишка...
Я тебе не земляк, а в разговоре у тебя не выдерживает кишка...
Если не хочешь общаться не общайся,
Я на отдыхе не парюсь, и ты не парься...
Если я отдыхаю, просто не мешай мне отдыхать...
Почему мне постоянно приходится каких то пидаров отражать...
---
Москва, следи за базаром, ты не на базаре...
Вечер только в разгаре...
За арбузами сходи в магазин по маминой указке...
Ты не в сказке... И непрокатят отмазки...
---
Если прослушал, пока песенка звучала...
Игру не получится начать сначала...
И если геймовер, то "геймовер"...
Запусти эти строке на повторе...
```

* Это тоже от балды!

```
Предпочитаю слушать голос записанный на ленту.
И пусть на битах не ровно лежат твои фрагменты.
Предпочитаю звук из мелодии на старую ак(у)стику.../ваше благородие/
Мона (на бабине) лучше чем... (твои) на (айтюнсе) рит(у)зики...
---
Ну что нам вечно дрочить на подмену Кабзона и Пугачевой ...
На фальшивые голоса плохой погоды, Тарзана и королевой ..
А так же на "децла" младшего и старшего, и рифмы Лигалайза...
На сговор на батлах, на то как Гуфа бадает Айза...
На ломанный слог, отсутствие рифмы, на старую школу и новую..
(Без музла, задержку)Диски, пластинки, флешки, цифра в ВК, (а тут запуск) выбираю пиратку совковую...
---
В старой школе и новой не вижу разницы...
Одни ....ут, другие под фанеру дразняться...
Одни на разогреве, вылизывают жёстко...
Те же 40 летние блестки...
Без пряника (в отместку) мой реп 10 лет без перебоя хлесткий...
---
Как и прежде открываю двери ногами... так отец научил.
В отличии... от мамочками обиженных с детство девочек, меня отец учил а не бил.
Я до сих пор в хуй не ставлют целочек и учителей, московских воспитанников и их детей!
---
В хуй не ставлю...
Не ставлю в хуй...
А ты там в микро дуй не дуй...
Я не ставлю тебя в хуй...
---
Кто бы не победил будем драться...
Пиздуны в этой стране буду жёстко караться...
Как и шалавы... Ни кем не будут восприниматься...
И тут никого не ебет как вы ради хайпа любите сраться...
На базаре ссаться...
---
И мне похуй что Тимати заплатил за хайп с баксером...
Для битвы на Донбассе Путин обратился к Архангельску, Дагестану, чеченцам и шахтёрам...
Никто не верит "ссыкухам и их ссыкухам"...
А также белухам, свинюхам и мухам...
---
Шоубиз с квадрокоптерами...
Прокачу вас на тракторе, по кочкам на фаркопе....
Сучня что съебалась за кардон, вас ту не ждут...
- И Гитлер кстати капут!
```
